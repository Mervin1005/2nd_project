### A Pluto.jl notebook ###
# v0.19.3

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local iv = try Base.loaded_modules[Base.PkgId(Base.UUID("6e696c72-6542-2067-7265-42206c756150"), "AbstractPlutoDingetjes")].Bonds.initial_value catch; b -> missing; end
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : iv(el)
        el
    end
end

# ╔═╡ cde2fc45-03a4-4c37-be1a-689f6f2613de
using Pkg

# ╔═╡ a3bf3a4f-9e0c-4f80-ba94-dea3c27470e7
using Markdown

# ╔═╡ 63ebac79-c358-418c-b50d-d5af0e87564a
using InteractiveUtils

# ╔═╡ f296c746-494a-4b98-8980-59a32cea7692
using PlutoUI

# ╔═╡ d4baeade-6c49-4fe5-91cd-d761f3e199b1
using DataStructures

# ╔═╡ 69d1dbab-c204-45ec-b9ca-07250d0e825c
macro bind(def, element)
    quote
        local iv = try Base.loaded_modules[Base.PkgId(Base.UUID("6e696c72-6542-2067-7265-42206c756150"), "AbstractPlutoDingetjes")].Bonds.initial_value catch; b -> missing; end
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : iv(el)
        el
    end
end

# ╔═╡ 03a1aef3-7b69-4e41-91f1-490f0d53f68c
# State definitions

# ╔═╡ ba58ffc2-2b14-4809-84fd-74ebf37eac92
struct State
name::String
position::Int64
parcel::Vector{Bool}
end

# ╔═╡ 6095ebf1-090c-438c-8935-a33eb58bf38b
# Action Definition

# ╔═╡ 699e208c-929f-44f1-a609-9421787438f7
struct Action
name::String
cost::Int64
end

# ╔═╡ 334eb81a-823d-48df-a2f6-38180e18ca80
A1 = Action("MW", 2)

# ╔═╡ 44ea50f3-8f6c-4e35-9d50-7f32736bbccb
A2 = Action("ME", 2)

# ╔═╡ 018bbad4-05f7-4777-9695-7993c735b599
A3 = Action("MU", 1)

# ╔═╡ 5677fdc4-e0e7-4807-9b55-122807812b7b
A4 = Action("MD", 1)

# ╔═╡ aa642bc0-68ad-42a9-a5b3-e25236173cba
A5 = Action("CO", 5)

# ╔═╡ ed5fccfb-1b04-4f6f-9b96-6c1403404051
S1 = State("State one", 1, [false, true, true, true,true])

# ╔═╡ b4f7728d-5f0e-4125-8746-7731745c8aa5
S2 = State("State two", 2, [true, true, true, true, true])

# ╔═╡ 958f6261-ccde-45a4-9f80-b11ad0183550
S3 = State("State three", 3, [true, false, true, true, true])

# ╔═╡ 22587cf3-63cf-4833-98f6-92f192b6f5d9
S4 = State("State four", 1, [false, true, false, true, true])

# ╔═╡ ca16305e-45c8-46ed-8384-ceac3e88ad7e
S5 = State("State five", 2, [true, true, false, true, true])

# ╔═╡ 693c5d86-b6d3-4e60-a7fa-747e43be7ed4
S6 = State("State six", 3, [true, false, false, true, true])

# ╔═╡ 5eace73d-3876-494d-9fa5-4b9c09a4df29
S7 = State("State seven", 1, [false, true, true, false, true])

# ╔═╡ aac7fce9-9501-4d61-83b2-385077c11a74
S8 = State("State eight", 2, [true, true, true, false, true])

# ╔═╡ 2d38e1b0-f78d-4b5e-97c2-41400783b022
S9 = State("State nine", 3, [false, true, true, false, true])

# ╔═╡ 82ffdff7-d72e-42b4-a373-40b46454f9af
S10 = State("State ten", 3, [false, true, true, true, true])

# ╔═╡ 9eb81ef9-6bc5-439c-bfce-93d7abcc7440
S11 = State("State eleven", 3, [true, false, true, false, true])

# ╔═╡ e0268368-b681-4d26-8dfe-95ea3d4940fd
S12 = State("State twelve", 3, [false, false, false, false, false])

# ╔═╡ a439dae4-4a4c-4197-a0a3-db2a9aa09c7d
md"## transition model"

# ╔═╡ c6fa618d-690f-40d1-b810-3ed2cc8f9b2d
Transition_Mod = Dict()

# ╔═╡ 2f1192cc-2c27-4dc0-9abb-19f656a09bd2
md"## Add a mapping/push! to the transition Model "

# ╔═╡ c3e61d45-d640-465a-b40b-54068457d27b
push!(Transition_Mod, S1 => [(A2, S2), (A3, S4), (A4, S7), (A5, S1)])

# ╔═╡ a988e324-6f7b-4b83-852c-2d85def76e69
push!(Transition_Mod, S2 => [(A1, S10), (A2, S3), (A3, S5), (A4, S8), (A5, S2)])

# ╔═╡ d0294204-a0bf-4fc2-80d1-d2ec15f55017
push!(Transition_Mod, S3 => [(A1, S2), (A3, S6), (A4, S11), (A5, S3)])

# ╔═╡ c7b842a9-c200-4510-b841-29c60f98105f
push!(Transition_Mod, S4 => [(A2, S5), (A4, S9), (A5, S4)])

# ╔═╡ eb98aa61-f303-499d-9600-e745d2ceecf7
push!(Transition_Mod, S5 => [(A1, S4), (A2, S6), (A4, S8), (A5, S5)])

# ╔═╡ 522b85b6-6200-4167-9a66-e22b590ae143
push!(Transition_Mod, S6 => [(A1, S5), (A2, S6), (A3, S7), (A3, S7)])

# ╔═╡ b06c958c-f8fb-40f1-8d3f-32d0fed59d9b
push!(Transition_Mod, S7 => [(A2, S8), (A3, S4), (A5, S7)])

# ╔═╡ bd805fdd-fdcf-46e5-9842-34adafc5845b
push!(Transition_Mod, S8 => [(A1, S7), (A2, S12), (A3, S5), (A5, S8)])

# ╔═╡ 20888310-88fc-4401-b76c-1aff864f9b8b
push!(Transition_Mod, S9 => [(A2, S12), (A2, S8), (A3, S4), (A5, S9)])

# ╔═╡ 5a130107-6535-44b8-8ca2-efc8648a33bc
push!(Transition_Mod, S10 => [(A2, S2), (A3, S4), (A4, S7), (A5, S10)])

# ╔═╡ 158353f7-5c65-4760-90a3-ec26a51ce0fc
push!(Transition_Mod, S11 => [(A1, S7), (A3, S6), (A5, S11)])

# ╔═╡ 8a781abb-ef5c-4a80-b4eb-50e7c5578a91
push!(Transition_Mod, S12 => [(A1, S7), (A2, S6), (A5, S12)])

# ╔═╡ 17969f53-0380-4b0f-95d1-0553df40d398
Transition_Mod

# ╔═╡ 27d05eb6-2e25-426b-8b38-06cdc2be9567
#Promptint user to enter data

# ╔═╡ 87a432d8-42af-4244-9227-e3dbb41a4667
@bind storey_no TextField()

# ╔═╡ 4d55ba04-6e07-4036-b052-eb6708f32626
with_terminal() do
	println( storey_no)
end

# ╔═╡ f7d4052b-322f-4499-bd62-df0298a58c7f
@bind no_off_offices TextField()

# ╔═╡ 8e5d2600-7096-4626-ab89-fbfe6bada31e
with_terminal() do
	println( no_off_offices)
end

# ╔═╡ fa650742-10f0-4ca0-a945-bffc390ef299
# Goal State

# ╔═╡ b119a771-a27f-4f1e-8e62-136b7ef39d1e
with_terminal() do
	println(S12)
end

# ╔═╡ 3aada344-eb31-445e-912b-220f60d7236f
function A_Star_Search(TransModel,initialState, goalState)
	
    result = []
	frontier = Queue{State}()
	visited = []
	parents = Dict{State, State}()
	first_state = true
    enqueue!(frontier, initialState)
	parent = initialState
	
    while true
		if isempty(frontier)
			return []
		else
			currentState = dequeue!(frontier)
			push!(visited, currentState)
			candidates = TransModel[currentState]
			for single_candidate in candidates
				if !(single_candidate[2] in visited)
					push!(parents, single_candidate[2] => currentState)
					if (single_candiadte[2] in goal_state)
						return get_result(TransModel, parents,initialState, single_candidate[2])
					else
						enqueue!(frontier, single_candidate[2])
					end
				end
			end
		end
	end
end

# ╔═╡ dd443069-b8c8-4a85-b716-d951711d2261
function create_result(TransModel, ancestors, initialState, goalState)
	result = []
	visitor = goalState
	while !(visitor == initialState)
		current_state_ancestor = ancestors[visitor]
		related_transitions = TransModel[current_state_ancestor]
		for single_trans in related_transitions
			if single_trans[2] == visitor
				push!(result, single_trans[1])
				break
			else
				continue
			end
		end
		visitor = current_state_ancestor
	end
	return result
end

# ╔═╡ d4fc90bb-f2c7-4a3d-9c20-d2e42e0955b3
function search(initialState, transition_dict, is_goal,all_candidates,
add_candidate, remove_candidate)
	visited = []
	ancestors = Dict{State, State}()
	the_candidates = add_candidate(all_candidates, initialState, 0)
	parent = initialState
	while true
		if isempty(the_candidates)
			return []
		else
			(t1, t2) = remove_candidate(the_candidates)
			current_state = t1
			the_candidates = t2
#proceed with handling the current state
			push!(visited, current_state)
			candidates = transition_dict[current_state]
			for single_candidate in candidates
				if !(single_candidate[2] in visited)
					push!(ancestors, single_candidate[2] => current_state)
					if (is_goal(single_candidate[2]))
						return create_result(transition_dict, ancestors,
initialState, single_candidate[2])
					else
						the_candidates = add_candidate(the_candidates,
single_candidate[2], single_candidate[1].cost)
						end
					end
				end
			end
		end
end

# ╔═╡ 2f0c981b-e7db-4aa2-93da-bdf2995693c6
function add_to_queue(queue::Queue{State}, state::State, cost::Int64)
    enqueue!(queue, state)
    return queue
end

# ╔═╡ 48d77ee2-5a3f-45f2-a54a-2488b9edd71b
function add_to_stack(stack::Stack{State}, state::State, cost::Int64)
    push!(stack, state)
    return stack
end

# ╔═╡ 60b7ee3a-5c1b-4a97-8721-bbf19c5ac8a9
function remove_from_queue(queue::Queue{State})
    removed = dequeue!(queue)
    return (removed, queue)
end

# ╔═╡ 1eb027c9-ceae-49f8-80a1-cf1f7f8ca396
function remove_from_stack(stack::Stack{State})
    removed = pop!(stack)
    return (removed, stack)
end

# ╔═╡ 98456af0-1a9a-40e9-b1b1-3d717139eb11
function goal_test(currentState::State)
    return ! (currentState.parcel[1] || currentState.parcel[2])
end

# ╔═╡ 5649d353-d8e8-4033-9933-cbc50f1cf962
# Calling A Star Search Strategy

# ╔═╡ 52d9fdf4-6bb0-4e7c-addf-7a905b2663ce
search(S2, Transition_Mod, goal_test, Queue{State}(), add_to_queue, remove_from_queue)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
DataStructures = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
InteractiveUtils = "b77e0a4c-d291-57a0-90e8-8db25a27a240"
Markdown = "d6f4376e-aef5-505a-96c1-9c027394607a"
Pkg = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"
PlutoUI = "7f904dfe-b85e-4ff6-b463-dae2292396a8"

[compat]
DataStructures = "~0.18.11"
PlutoUI = "~0.7.38"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[[deps.AbstractPlutoDingetjes]]
deps = ["Pkg"]
git-tree-sha1 = "8eaf9f1b4921132a4cff3f36a1d9ba923b14a481"
uuid = "6e696c72-6542-2067-7265-42206c756150"
version = "1.1.4"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.ColorTypes]]
deps = ["FixedPointNumbers", "Random"]
git-tree-sha1 = "024fe24d83e4a5bf5fc80501a314ce0d1aa35597"
uuid = "3da002f7-5984-5a60-b8a6-cbb66c0b333f"
version = "0.11.0"

[[deps.Compat]]
deps = ["Base64", "Dates", "DelimitedFiles", "Distributed", "InteractiveUtils", "LibGit2", "Libdl", "LinearAlgebra", "Markdown", "Mmap", "Pkg", "Printf", "REPL", "Random", "SHA", "Serialization", "SharedArrays", "Sockets", "SparseArrays", "Statistics", "Test", "UUIDs", "Unicode"]
git-tree-sha1 = "b153278a25dd42c65abbf4e62344f9d22e59191b"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "3.43.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "3daef5523dd2e769dad2365274f760ff5f282c7d"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.11"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.FixedPointNumbers]]
deps = ["Statistics"]
git-tree-sha1 = "335bfdceacc84c5cdf16aadc768aa5ddfc5383cc"
uuid = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"
version = "0.8.4"

[[deps.Hyperscript]]
deps = ["Test"]
git-tree-sha1 = "8d511d5b81240fc8e6802386302675bdf47737b9"
uuid = "47d2ed2b-36de-50cf-bf87-49c2cf4b8b91"
version = "0.0.4"

[[deps.HypertextLiteral]]
git-tree-sha1 = "2b078b5a615c6c0396c77810d92ee8c6f470d238"
uuid = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
version = "0.9.3"

[[deps.IOCapture]]
deps = ["Logging", "Random"]
git-tree-sha1 = "f7be53659ab06ddc986428d3a9dcc95f6fa6705a"
uuid = "b5f81e59-6552-4d32-b1f0-c071b021bf89"
version = "0.2.2"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "3c837543ddb02250ef42f4738347454f95079d4e"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.3"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Parsers]]
deps = ["Dates"]
git-tree-sha1 = "1285416549ccfcdf0c50d4997a94331e88d68413"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.3.1"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.PlutoUI]]
deps = ["AbstractPlutoDingetjes", "Base64", "ColorTypes", "Dates", "Hyperscript", "HypertextLiteral", "IOCapture", "InteractiveUtils", "JSON", "Logging", "Markdown", "Random", "Reexport", "UUIDs"]
git-tree-sha1 = "670e559e5c8e191ded66fa9ea89c97f10376bb4c"
uuid = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
version = "0.7.38"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╠═a3bf3a4f-9e0c-4f80-ba94-dea3c27470e7
# ╠═63ebac79-c358-418c-b50d-d5af0e87564a
# ╠═69d1dbab-c204-45ec-b9ca-07250d0e825c
# ╠═cde2fc45-03a4-4c37-be1a-689f6f2613de
# ╠═f296c746-494a-4b98-8980-59a32cea7692
# ╠═d4baeade-6c49-4fe5-91cd-d761f3e199b1
# ╠═03a1aef3-7b69-4e41-91f1-490f0d53f68c
# ╠═ba58ffc2-2b14-4809-84fd-74ebf37eac92
# ╠═6095ebf1-090c-438c-8935-a33eb58bf38b
# ╠═699e208c-929f-44f1-a609-9421787438f7
# ╠═334eb81a-823d-48df-a2f6-38180e18ca80
# ╠═44ea50f3-8f6c-4e35-9d50-7f32736bbccb
# ╠═018bbad4-05f7-4777-9695-7993c735b599
# ╠═5677fdc4-e0e7-4807-9b55-122807812b7b
# ╠═aa642bc0-68ad-42a9-a5b3-e25236173cba
# ╠═ed5fccfb-1b04-4f6f-9b96-6c1403404051
# ╠═b4f7728d-5f0e-4125-8746-7731745c8aa5
# ╠═958f6261-ccde-45a4-9f80-b11ad0183550
# ╠═22587cf3-63cf-4833-98f6-92f192b6f5d9
# ╠═ca16305e-45c8-46ed-8384-ceac3e88ad7e
# ╠═693c5d86-b6d3-4e60-a7fa-747e43be7ed4
# ╠═5eace73d-3876-494d-9fa5-4b9c09a4df29
# ╠═aac7fce9-9501-4d61-83b2-385077c11a74
# ╠═2d38e1b0-f78d-4b5e-97c2-41400783b022
# ╠═82ffdff7-d72e-42b4-a373-40b46454f9af
# ╠═9eb81ef9-6bc5-439c-bfce-93d7abcc7440
# ╠═e0268368-b681-4d26-8dfe-95ea3d4940fd
# ╠═a439dae4-4a4c-4197-a0a3-db2a9aa09c7d
# ╠═c6fa618d-690f-40d1-b810-3ed2cc8f9b2d
# ╠═2f1192cc-2c27-4dc0-9abb-19f656a09bd2
# ╠═c3e61d45-d640-465a-b40b-54068457d27b
# ╠═a988e324-6f7b-4b83-852c-2d85def76e69
# ╠═d0294204-a0bf-4fc2-80d1-d2ec15f55017
# ╠═c7b842a9-c200-4510-b841-29c60f98105f
# ╠═eb98aa61-f303-499d-9600-e745d2ceecf7
# ╠═522b85b6-6200-4167-9a66-e22b590ae143
# ╠═b06c958c-f8fb-40f1-8d3f-32d0fed59d9b
# ╠═bd805fdd-fdcf-46e5-9842-34adafc5845b
# ╠═20888310-88fc-4401-b76c-1aff864f9b8b
# ╠═5a130107-6535-44b8-8ca2-efc8648a33bc
# ╠═158353f7-5c65-4760-90a3-ec26a51ce0fc
# ╠═8a781abb-ef5c-4a80-b4eb-50e7c5578a91
# ╠═17969f53-0380-4b0f-95d1-0553df40d398
# ╠═27d05eb6-2e25-426b-8b38-06cdc2be9567
# ╠═87a432d8-42af-4244-9227-e3dbb41a4667
# ╠═4d55ba04-6e07-4036-b052-eb6708f32626
# ╠═f7d4052b-322f-4499-bd62-df0298a58c7f
# ╠═8e5d2600-7096-4626-ab89-fbfe6bada31e
# ╠═fa650742-10f0-4ca0-a945-bffc390ef299
# ╠═b119a771-a27f-4f1e-8e62-136b7ef39d1e
# ╠═3aada344-eb31-445e-912b-220f60d7236f
# ╠═dd443069-b8c8-4a85-b716-d951711d2261
# ╠═d4fc90bb-f2c7-4a3d-9c20-d2e42e0955b3
# ╠═2f0c981b-e7db-4aa2-93da-bdf2995693c6
# ╠═48d77ee2-5a3f-45f2-a54a-2488b9edd71b
# ╠═60b7ee3a-5c1b-4a97-8721-bbf19c5ac8a9
# ╠═1eb027c9-ceae-49f8-80a1-cf1f7f8ca396
# ╠═98456af0-1a9a-40e9-b1b1-3d717139eb11
# ╠═5649d353-d8e8-4033-9933-cbc50f1cf962
# ╠═52d9fdf4-6bb0-4e7c-addf-7a905b2663ce
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
